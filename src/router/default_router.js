import { HomeLayout } from '@/layouts'

/* 设计器*/
import designer from '@/pages/designer/index'

export default [
  {
    id: '1',
    meta: { title: '表单设计器' },
    name: 'designer',
    component: HomeLayout,
    path: '/',
    redirect: '/designer/index',
    children: [
      {
        id: '2-1',
        path: '/designer/index',
        name: 'designerIndex',
        component: designer,
        meta: { title: '表单设计器' }
      }
    ]
  }
]
