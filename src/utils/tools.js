/**
* @Author : liu.q [916000612@qq.com]
* @Date : 2020/8/1 7:17 下午
* @Description : 获取随机id
*/
export function widgetRandomId(length) {
  let str = ''
  for (var i = 1; i <= length; i++) {
    str += 'x'
  }
  return str.replace(/[xy]/g, function(c) {
    const r = Math.random() * 10 | 0
    const v = c === 'x' ? r : (r & 0x3 | 0x8)
    return v.toString(16)
  })
}
/**
* @Author : liu.q [916000612@qq.com]
* @Date : 2020/8/1 7:17 下午
* @Description : 格式化日期
*/
export function formatDate(date, fmt) {
  try {
    if (!(date instanceof Date)) {
      date = new Date(date)
    }
    if (fmt === undefined || fmt === '') {
      fmt = 'yyyy-MM-dd hh:mm:ss'
    }
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
    }
    const o = {
      'M+': date.getMonth() + 1,
      'd+': date.getDate(),
      'h+': date.getHours(),
      'm+': date.getMinutes(),
      's+': date.getSeconds()
    }
    for (const k in o) {
      if (new RegExp(`(${k})`).test(fmt)) {
        const str = o[k] + ''
        fmt = fmt.replace(RegExp.$1, RegExp.$1.length === 1 ? str : padLeftZero(str))
      }
    }
    return fmt
  } catch (e) {
    console.log(e)
    return '-'
  }
}

function padLeftZero(str) {
  return ('00' + str).substr(str.length)
}
